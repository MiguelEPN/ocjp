/**
 * Created by Miguel on 22/05/17.
 */
public class Persona {

    private String nombre;
    private String apellidos;
    private int edad;


    public Persona (){}     //Constructor vacío

    public Persona (String nombre, String apellidos, int edad) {       //Constructor

        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;                   }

    public String getNombre () { return nombre;  }
    public String getApellidos () { return apellidos;  }
    public int getEdad () { return edad;   }

}
