/**
 * Created by Miguel on 08/05/17.
 */
public class ClassAuto {    // modificador public
    public class Auto {

        int kilometraje;  //modificador default

        public Auto(String marca) {
            // El constructor tiene solo un parametro, en este caso marca
            System.out.println("La marca es : " + marca);
        }

        public void setKilometraje(int kilometraje) {
            this.kilometraje = kilometraje;
        }

        public int getKilometraje() {
            System.out.println("El kilometraje es : " + kilometraje);
            return this.kilometraje;
        }
    }
}

