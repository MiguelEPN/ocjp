package com.company;

import com.company.Frutas.*;

public class Main {

    public static void main(String[] args) {
        String salida = " ";
        Object o = new Object();
        Fruit f = new Fruit();
        Apple a = new Apple();
        Citrus c = new Citrus();
        Orange orange = new Orange();
        Squeezable s = null;

        salida = "Object ";
        try{
            o = (Fruit)f;
            salida = salida +"OKFruit: ";
        }catch(Exception o1){salida = salida +"NOFruit: ";}
        try{
            o = (Apple)a;
            salida = salida +"OKApple: ";
        }catch(Exception f1){ salida = salida +"NOApple: ";}
        try{
            o = (Squeezable)s;
            salida = salida +"OKSqueezable: ";
        }catch(Exception m1){salida = salida +"NOSqueezable: ";}
        try{
            o = (Citrus)c;
            salida = salida +"OKCitrus: ";
        }catch(Exception c1){salida = salida +"NOCitrus: ";}
        try{
            o = (Orange)orange;
            salida = salida +"OKOrange: \n";
        }catch(Exception n1){salida = salida +"NOOrange: \n";}

        salida = salida +  "Fruit ";
        try{
            f = (Fruit)f;
            salida = salida +"OKFruit: ";
        }catch(Exception o1){salida = salida +"NOFruit: ";}
        try{
            f = (Apple)a;
            salida = salida +"OKApple: ";
        }catch(Exception f1){ salida = salida +"NOApple: ";}
        try{
            f = (Fruit)s;
            salida = salida +"OKSqueezable: ";
        }catch(Exception m1){salida = salida +"NOSqueezable: ";}
        try{
            f = (Citrus)c;
            salida = salida +"OKCitrus: ";
        }catch(Exception c1){salida = salida +"NOCitrus: ";}
        try{
            f= (Orange)orange;
            salida = salida +"OKOrange: \n";
        }catch(Exception n1){salida = salida +"NOOrange: \n";}

        salida = salida +  "Apple ";
        try{
            a = (Apple) f;
            salida = salida +"OKFruit: ";
        }catch(Exception o1){salida = salida +"NOFruit: ";}
        try{
            a = (Apple)a;
            salida = salida +"OKApple: ";
        }catch(Exception f1){ salida = salida +"NOApple: ";}

        salida = salida +  "Citrus ";

        try{
            c = (Citrus)c;
            salida = salida +"OKCitrus: ";
        }catch(Exception c1){salida = salida +"NOCitrus: ";}
        try{
            c= (Orange)orange;
            salida = salida +"OKOrange: \n";
        }catch(Exception n1){salida = salida +"NOOrange: \n";}

        salida = salida +  "Orange ";

        try{
            orange = (Orange) s;
            salida = salida +"OKSqueezable: ";
        }catch(Exception m1){salida = salida +"NOSqueezable: ";}
        try{
            orange = (Orange) c;
            salida = salida +"OKCitrus: ";
        }catch(Exception c1){salida = salida +"NOCitrus: ";}
        try{
            orange = (Orange)orange;
            salida = salida +"OKOrange: \n";
        }catch(Exception n1){salida = salida +"NOOrange: \n";}

        System.out.println(salida);
    }
}
