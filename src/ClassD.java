/**
 * Created by Miguel on 08/05/17.
 */
public class ClassD {
    protected void ejemplo(){
        System.out.print("Ejemplo con modificador protected"); // protected
    }
}

class ClassE  {
    final double pi=3.1415;    //ejemplo de final
    void imprimir(){
       // pi = 2.6; no se podría alterar el valor de pi, acá daría error
        System.out.print(pi);
    }
}

